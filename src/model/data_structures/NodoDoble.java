package model.data_structures;

public class NodoDoble<T> {


	private T item;
	private NodoDoble<T> next;
	private NodoDoble<T> prev;

	public NodoDoble(T value)
	{
		item=value;
		next = null;
		prev = null;
	}

	public T giveItem()
	{
		return item;
	}

	public NodoDoble<T> getNext()
	{
		return next;
	}

	public NodoDoble<T> getPrev()
	{
		return prev;
	}

	public void setItem(T value)
	{
		item=value;
	}
	public void setNext(NodoDoble<T> first)
	{
		next=first;
	}
	public void setPrev(NodoDoble<T> actual)
	{
		prev=actual;
	}
	

}

