package model.data_structures;

	
import java.util.Iterator;

public class Stack<T> {

	private int top;

	private ListaDobleEncadenada<T> lista;

	public Stack() {
		top = -1;
		lista = new ListaDobleEncadenada<T>();
	}

	public void push(T pushValue) {
		lista.agregarElementoFinal(pushValue);
		top ++;
	}

	public T pop() throws Exception{

		if (top == -1) {
			throw new Exception("La pila se encunetra vac�a");
		}

		T item = null;

		item = lista.darElemento(top);
		lista.eliminarElemento(top);

		top --;

		return item;
	}
	public int size()
	{
		return lista.darNumeroElementos();
	}

	public boolean isEmpty()
	{
		return lista.isEmpty();
	}
	public T peek() {
		return lista.darElementoPosicionActual();
	}

}
