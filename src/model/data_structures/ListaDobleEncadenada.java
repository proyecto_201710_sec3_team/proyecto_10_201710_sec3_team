package model.data_structures;

import java.util.Iterator;


import java.util.NoSuchElementException;

public class ListaDobleEncadenada<T> implements ILista<T> {

	private NodoDoble<T> first;

	private NodoDoble<T> actual;

	private NodoDoble<T> last;

	private int size;

	public ListaDobleEncadenada()
	{
		size = 0;
	}

	public boolean isEmpty(){return size == 0; }

	
	public Iterator<T> iterator() {
		return new DoublyLinkedListIterator<T>();

	}


	private class DoublyLinkedListIterator<T> implements Iterator<T> {
		private NodoDoble<T> ante;

		public DoublyLinkedListIterator()
		{
			ante = new NodoDoble<T>(null);
			ante.setNext((NodoDoble<T>) first);
		}

		public boolean hasNext()      { if (ante.getNext() !=null) {return true;}else{return false;}}

		public T next() {
			if (!hasNext()){ throw new NoSuchElementException( "No hay elemento siguiente");}
			ante = ante.getNext();
			T item = ante.giveItem();
			return item;
		}

		@Override
		public void remove() {
		}


	}

	public void agregarElementoFinal(T elem) {
		NodoDoble<T> newnode = new NodoDoble<T>(elem);
		if(first==null){
			first=newnode;
			actual=newnode;
			last=newnode;
			newnode.setNext(null);
			newnode.setPrev(null);
		}else{
			newnode.setPrev(last);
			last.setNext(newnode);
			last=newnode;
		}
		size++;
	}

	public T darElemento(int pos) {
		Iterator<T> it = iterator();
		int i=0;
		T rta=null;
		while(it.hasNext() && i<=pos)
		{
			rta=it.next();
			if(i==pos)
			{
				return rta;
			}
			i++;
		}
		return rta;
	}


	public int darNumeroElementos() {
		return size;
	}

	public T darElementoPosicionActual() throws NullPointerException{
		return actual.giveItem();
	}

	public boolean avanzarSiguientePosicion() {
		if(actual==null)return false;
		else if(actual.getNext()!=null)
		{
			actual=actual.getNext();
			return true;
		}
		else
		{
			return false;
		}
	}

	public boolean retrocederPosicionAnterior() {
		if (actual == null) {return false;}
		else if (actual.getPrev() != null){actual = actual.getPrev(); return true;}
		else {return false;}
	}

	public NodoDoble<T> getFirst()
	{
		return first;
	}

	public NodoDoble<T> getActual()
	{
		return actual;
	}

	public void resetActual()
	{
		actual=first;
	}

	public void increase()
	{
		size++;
	}

	public void setActual(NodoDoble<T> x) {

		actual=x;
	}

	public void setFirst(NodoDoble<T> x)
	{
		first=x;
	}
	
	public void eliminarContenido()
	{
		first=null;
		actual=null;
		last=null;
	}
	public void eliminarElemento(int pos){
		if (first == null) {
			return;
		}
		
		//Creo que tiene un error al eliminar al final pero no lo veo, tal vez t� lo puedes encontrar.

		if (pos == 0) {
			first = first.getNext();
			if (first == null) {
				last = null;
			}
		}

		else{
			NodoDoble<T> temp = first;
			for (int index = 0; index < pos; index++){
				temp = temp.getNext();
			}
			if(temp!=last) 	temp.getNext().setPrev(temp.getPrev());
			else last=temp.getPrev();
			temp.getPrev().setNext(temp.getNext());
		}
		size--;
	}
	
	public static void main(String[] args) {
		ListaDobleEncadenada<String> x= new ListaDobleEncadenada<String>();
		x.agregarElementoFinal("A");
		int fin=x.darNumeroElementos()-1;
		x.eliminarElemento(fin);
		System.out.println(x.isEmpty());
		x.agregarElementoFinal("A");
		x.agregarElementoFinal("B");
		x.agregarElementoFinal("C");
		fin=x.darNumeroElementos()-1;
		System.out.println(fin);
		System.out.println(x.darElemento(fin));
		x.eliminarElemento(fin);
		fin=x.darNumeroElementos()-1;
		System.out.println(x.darElemento(fin));

	}
}

