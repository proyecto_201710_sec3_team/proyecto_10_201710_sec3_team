package model.logic;

import java.io.BufferedReader;



import java.io.File;
import java.io.FileReader;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import api.ISistemaRecomendacionPeliculas;
import model.data_structures.ILista;

import model.data_structures.ListaDobleEncadenada;
import model.data_structures.Stack;
import model.vo.VOGeneroPelicula;
import model.vo.VOGeneroTag;
import model.vo.VOGeneroUsuario;
import model.vo.VOOperacion;
import model.vo.VOPelicula;
import model.vo.VOPeliculaPelicula;
import model.vo.VOPeliculaUsuario;
import model.vo.VORating;
import model.vo.VOTag;
import model.vo.VOUsuario;
import model.vo.VOUsuarioConteo;
import model.vo.VOUsuarioGenero;
import sun.applet.Main;

public class SistemaRecomendacionPeliculas implements ISistemaRecomendacionPeliculas {

	private ListaDobleEncadenada<VOPelicula> misPeliculas;
	
	private ListaDobleEncadenada<VOGeneroPelicula> generoPeliculas;
	
	private ListaDobleEncadenada<VOUsuario> usuarios;
	
	private ListaDobleEncadenada<VOTag> tags;
	
	private ListaDobleEncadenada<VORating> ratings;
	
	private ListaDobleEncadenada<VOGeneroUsuario> generoUsuarios;//uiguig
	
	private ListaDobleEncadenada<VOPeliculaUsuario> usuariosSim;
	
	private ListaDobleEncadenada<VOPeliculaPelicula> recUsuario;
	
	private ListaDobleEncadenada<VOUsuarioGenero> usuariosGenero;//uihuih
	
	private Stack<VOOperacion> operaciones;
	
	private ListaDobleEncadenada<VOOperacion> reciente;
	
	public void crearNuevoSR()
	{
		misPeliculas=new ListaDobleEncadenada<VOPelicula>();
		generoPeliculas=new ListaDobleEncadenada<VOGeneroPelicula>();
		usuarios= new ListaDobleEncadenada<VOUsuario>();
		tags= new ListaDobleEncadenada<VOTag>();
		ratings= new ListaDobleEncadenada<VORating>();
		generoUsuarios= new ListaDobleEncadenada<VOGeneroUsuario>();
		usuariosSim= new ListaDobleEncadenada<VOPeliculaUsuario>();
		recUsuario= new ListaDobleEncadenada<VOPeliculaPelicula>();
		usuariosGenero= new ListaDobleEncadenada<VOUsuarioGenero>();
		operaciones= new Stack<VOOperacion>();
		reciente= new ListaDobleEncadenada<VOOperacion>();
	}
	/**
	 * Carga una lista de pel�culas del archivo rutaPeliculas<br>
	 * @param rutaPeliculas ruta del archivo con listado de pel�culas 
	 * 		  en formato csv
	 * @return true si se pudo leer el archivo
	 */
	public boolean cargarPeliculasSR(String rutaPeliculas)
	{
		misPeliculas= new ListaDobleEncadenada<VOPelicula>();
		generoPeliculas= new ListaDobleEncadenada<VOGeneroPelicula>();
		try
		{
			FileReader fr= new FileReader(new File(rutaPeliculas));
			BufferedReader br = new BufferedReader(fr);
			ListaDobleEncadenada<String> temp = null;
			br.readLine();
			String linea=br.readLine();
			Pattern pattern=Pattern.compile("\\(([^)]+)\\)");
			String[] datos=null;
			String fuente=null;
			while(linea!=null)
			{
				 datos=linea.split(",");
				int op=2;
				 fuente=datos[1];
				while(op<datos.length-1)
				{
					fuente+=","+datos[op];
					op++;
				}
				//-------
				int anho=0;
				 Matcher m = pattern.matcher(fuente);
			     while(m.find()) {
			    	 
			    		 if(isNumeric(m.group(1).split("-")[0]))
				    	 {
			    			 if(m.group(1).split("-").length!=1)
			    			 {
			    				 if(isNumeric(m.group().split("-")[1]) || m.group().split("-")[1].equals(""))
			    				 {
			    					 anho=Integer.parseInt(m.group(1).split("-")[0]);
						    		 fuente=fuente.replaceAll("\\("+m.group(1)+"\\)"," ");
						    		 break;
			    				 }
			    			 }
			    			 else
			    			 {
			    				 anho=Integer.parseInt(m.group(1).split("-")[0]);
					    		 fuente=fuente.replaceAll("\\("+m.group(1)+"\\)"," ");
					    		 break;
			    			 }
				    		 
				    	 }
			     }
			     //--------------------
			     if(isNumeric(datos[op]))
			     {
			    	 return false;
			     }
				String[] generos=datos[op].split("\\|");
				temp= new ListaDobleEncadenada<String>();
				for(String t:generos)
				{
					temp.agregarElementoFinal(t);
				}
				VOPelicula x = new VOPelicula();
				x.setId(Integer.parseInt(datos[0]));
				x.setAgnoPublicacion(anho);
				x.setGenerosAsociados(temp);
				x.setTitulo(fuente);
				x.setTagsAsociados(new ListaDobleEncadenada<String>());
				System.out.println(linea);
				misPeliculas.agregarElementoFinal(x);	
				crearPeliculasGenero(x);
				linea=br.readLine();
				int k=0;
				k++;
			}
			br.close();
			fr.close();
			System.out.println("TERMINÉ");
		}
		catch (Exception e)
		{
			misPeliculas.eliminarContenido();
			generoPeliculas.eliminarContenido();
			generoUsuarios.eliminarContenido();
			return false;
		}
		return true;
	}
	
	//Reparar método ya que tiene una complejidad de n^3
	private void crearPeliculasGenero(VOPelicula nueva) {
		Iterator<String> generos= nueva.getGenerosAsociados().iterator();
		Iterator<VOGeneroPelicula> iter=null;
		boolean agregado=false;
		String g=null;
		VOGeneroPelicula pel=null;
		ListaDobleEncadenada<Integer> p=null;
		ListaDobleEncadenada<VOUsuarioConteo> x=null;
		VOGeneroUsuario u=null;
		VOUsuarioConteo us=null;
		int id= nueva.getMovieID();
		while(generos.hasNext())
		{
			agregado=false;
			iter =generoPeliculas.iterator();
			 g= generos.next();
			while(iter.hasNext())
			{
				pel=iter.next();
				if(pel.getGenero().equals(g))
				{
					pel.getPeliculas().agregarElementoFinal(id);
					agregado=true;
					break;
				}
			}
			if(!agregado)
			{
				//Crea y agrega los géneros de película
				pel= new VOGeneroPelicula();
				pel.setGenero(g);
				p= new ListaDobleEncadenada<Integer>();
				p.agregarElementoFinal(id);
				pel.setPeliculas(p);
				generoPeliculas.agregarElementoFinal(pel);
				
				//Crea y agrega los géneros de usuario
				u= new VOGeneroUsuario();
				x= new ListaDobleEncadenada<VOUsuarioConteo>();
				us= new VOUsuarioConteo();
				x.agregarElementoFinal(us);
				u.setUsuarios(x);
				u.setGenero(g);
				generoUsuarios.agregarElementoFinal(u);
				
			}
		}
	}
	
	private boolean isNumeric(String str)  
	{  
	  try  
	  {  
	    double d = Double.parseDouble(str);  
	  }  
	  catch(NumberFormatException nfe)  
	  {  
	    return false;  
	  }  
	  return true;  
	}


	/**
	 * Carga el historial de ratings del archivo rutaRatings<br>
	 * @param rutaRatings ruta del archivo con el historial de ratings 
	 * 		  en formato csv
	 * @return true si se pudo leer el archivo
	 */
	public boolean cargarRatingsSR(String rutaRatings)
	{
		try
		{

			ratings=new ListaDobleEncadenada<VORating>();
			FileReader fr = new FileReader(rutaRatings);
			BufferedReader br = new BufferedReader(fr);
			br.readLine();
			long idActual=0;
			int suma=0;
			long time=0;
			String linea=br.readLine();
			System.out.println(linea);
			String[] datos=null;
			VORating r=null;
			VOUsuario u=null;
			while(linea!=null)
			{
				datos = linea.split(",");
				 r = new VORating();
				long idUsuario=Long.parseLong(datos[0]);
				int idPelicula=Integer.parseInt(datos[1]);
				if(idActual==0)
				{
					idActual=idPelicula;
				}
				if(idUsuario!=idActual)
				{
					u = new VOUsuario();
					u.setIdUsuario(idActual);
					u.setNumRatings(suma);
					u.setPrimerTimestamp(time);
					suma=0;
					usuarios.agregarElementoFinal(u);
					idActual=idUsuario;
				}
				double rating = Double.parseDouble(datos[2]);
				r.setIdPelicula(idPelicula);
				r.setIdUsuario(idUsuario);
				r.setRating(rating);
				long temp=Long.parseLong(datos[3]);
				r.setTime(temp);
				ratings.agregarElementoFinal(r);
				if(temp!=time || time==0)
				{
					time=temp;
				}
				nuevoRatingYConteo(idPelicula,rating,idUsuario);
				suma++;
				System.out.println(linea);
				linea=br.readLine();
			}
			br.close();
			fr.close();
			System.out.println("TERMINÉ");

		}
		catch (Exception e)
		{
			ratings.eliminarContenido();
			usuarios.eliminarContenido();
			generoUsuarios.eliminarContenido();
			return false;
		}
		return true;
	}

	private void nuevoRatingYConteo(int idPelicula, double rating , long usuarioId)
	{
		Iterator<VOPelicula> iter= misPeliculas.iterator();
		VOPelicula temp=null;
		VOPelicula p=null;
		while(iter.hasNext())
		{
			 p= iter.next();
			if(p.getMovieID()==idPelicula)
			{
				temp=p;
				break;
			}
		}
		//Agrega al usuario con su conteo por género
		if(temp!=null)
		{
			temp.setNumeroRatings(temp.getNumeroRatings()+1);
			temp.setPromedioRatings((temp.getPromedioRatings()+rating)/temp.getNumeroRatings());
			Iterator<String> genero= temp.getGenerosAsociados().iterator();
			Iterator<VOGeneroUsuario> g=null;
			String cadena=null;
			VOGeneroUsuario a=null;
			Iterator<VOUsuarioConteo> x=null;
			VOUsuarioConteo usuario=null;
			while(genero.hasNext())
			{
				cadena=genero.next();
				g= generoUsuarios.iterator();
				while(g.hasNext())
				{
					a=g.next();
					if(a.getGenero().equals(cadena))
					{
						boolean agregado=false;
						 x= a.getUsuarios().iterator();
						while(x.hasNext())
						{
							usuario= x.next();
							if(usuario.getIdUsuario()==usuarioId)
							{
								usuario.setConteo(usuario.getConteo()+1);
								agregado=true;
								break;
							}
							
						}
						if(!agregado)
						{
							usuario= new VOUsuarioConteo();
							usuario.setIdUsuario(usuarioId);
							usuario.setConteo(1);
							a.getUsuarios().agregarElementoFinal(usuario);
						}
					}
				}
			}
		}
		
	}
	
	/**
	 * Carga una lista de tags del archivo rutaTags
	 * @param rutaTags ruta del archivo de tags en formato csv
	 * @return true si se pudo leer el archivo
	 */
	public boolean cargarTagsSR(String rutaTags)
	{
		try
		{
			FileReader fr = new FileReader(rutaTags);
			BufferedReader br = new BufferedReader(fr);
			br.readLine();
			String linea=br.readLine();
			while(linea!=null)
			{
				String[] datos = linea.split(",");
				VOTag t = new VOTag();
				String fuente=datos[2];
				for(int i=3;i<datos.length-1;i++)
				{
					if(isNumeric(datos[i])) break;
					else
					{
						fuente+=","+datos[i];
					}
				}
				t.setTag(fuente);
				t.setTimestamp(Long.parseLong(datos[datos.length-1]));
				tags.agregarElementoFinal(t);
				int id=Integer.parseInt(datos[1]);
				long usId=Long.parseLong(datos[0]);
				System.out.println(linea);
				cargarTagsYGenero(id,fuente,usId);
				linea=br.readLine();
			}
			br.close();
			fr.close();
			System.out.println("TERMINÉ");

		}
		catch (Exception e)
		{
			tags.eliminarContenido();
			usuariosGenero.eliminarContenido();
			return false;
		}
		return true;
	}
	
	private void cargarTagsYGenero(int movieId, String tag, long usId)
	{
		Iterator<VOPelicula> mov= misPeliculas.iterator();
		VOPelicula p= null;
		while(mov.hasNext())
		{
			VOPelicula temp = mov.next();
			if(temp.getMovieID()==movieId)
			{
				temp.setNumeroTags(temp.getNumeroTags()+1);
				temp.getTagsAsociados().agregarElementoFinal(tag);
				p=temp;
				break;
			}
		}
		VOUsuarioGenero escogido=new VOUsuarioGenero();
		ListaDobleEncadenada<VOGeneroTag> x= new ListaDobleEncadenada<VOGeneroTag>();
		escogido.setListaGeneroTags(x);
		Iterator<VOUsuarioGenero> temp = usuariosGenero.iterator();
		escogido.setIdUsuario(usId);
		VOUsuarioGenero t=null;
		boolean agregado=false;
		while(temp.hasNext())
		{   
			t= temp.next();
			if(t.getIdUsuario()==usId)
			{
				escogido=t;
				agregado=true;
				break;
			}
		}
		//Se añaden los tags repetidos por género
		if(p!=null)
		{
			Iterator<String> generos=p.getGenerosAsociados().iterator();
			VOGeneroTag ac=null;
			String actual=null;
			boolean encontrado=false;
			while(generos.hasNext())
			{
				Iterator<VOGeneroTag> g= escogido.getListaGeneroTags().iterator();
				actual=generos.next();
				while(g.hasNext())
				{
					ac=g.next();
					if(actual.equals(ac.getGenero()))
					{
						ac.getTags().agregarElementoFinal(tag);
						encontrado=true;
						break;
					}
				}
				if(!encontrado)
				{
					ac=new VOGeneroTag();
					ac.setGenero(actual);
					ac.setTags(new ListaDobleEncadenada<String>());
					ac.getTags().agregarElementoFinal(tag);
					escogido.getListaGeneroTags().agregarElementoFinal(ac);
				}
			}
			if(!agregado)usuariosGenero.agregarElementoFinal(escogido);
		}
	}
	
	/**
	 * Retorna el n�mero de pel�culas registradas en el SR
	 * @return el n�mero de pel�culas que existen en el SR
	 */
	public int sizeMoviesSR()
	{
		return misPeliculas.darNumeroElementos();
	}
	
	/**
	 * Retorna el n�mero de usuarios registrados en el SR
	 * @return el n�mero de usuarios registrados en el SR
	 */
	public int sizeUsersSR()
	{
		return usuarios.darNumeroElementos();
	}
	
	/**
	 * Retorna el n�mero de tags registrados en el SR
	 * @return el n�mero de tags registrados en el SR
	 */
	public int sizeTagsSR()
	{
		return tags.darNumeroElementos();
	}
	
	/**
	 * Retorna las n pel�culas m�s populares por cada uno de los g�neros 
	 * @param n n�mero de pel�culas populares por g�nero a retornar
	 * @return Lista donde cada posici�n tiene un g�nero y asociado a este una 
	 * 		   lista de pel�culas populares
	 */
	public ILista<VOGeneroPelicula> peliculasPopularesSR (int n)
	{
		
	}
	
	/**
	 * Retorna una lista de pel�culas ordenadas por los siguientes criterios:<br>
	 * <ol> 
	 * 	<li>A�o de publicaci�n</li>
	 * 	<li>Promedio de ratings</li>
	 * 	<li>Orden alfab�tico</li>
	 * </ol> 
	 * @return lista de pel�culas ordenada por a�o, promedio y t�tulo
	 */
	public ILista<VOPelicula> catalogoPeliculasOrdenadoSR()
	{
		
	}
	
	/**
	 * Retorna  la mejor pel�cula de cada uno de los g�neros por su rating promedio
	 * @return Lista donde cada posici�n tiene un g�nero y asociado a este tiene
	 *         una lista de tama�o 1 con la mejor pel�cula del g�nero dado su rating promedio
	 */
	public ILista<VOGeneroPelicula> recomendarGeneroSR()
	{
		
	}
	
	/**
	 * Retorna una lista de pel�culas por cada uno de los g�neros, cada una de 
	 * las pel�culas tiene sus tags asociados
	 * @return lista de pel�culas por cada g�nero, cada pel�cula tiene sus tags asociados
	 */
	public ILista<VOGeneroPelicula> opinionRatingsGeneroSR()
	{
		
	}
	
	/**
	 * Retorna una lista con las pel�culas recomendadas seg�n los ratings 
	 * recibidos en el archivo rutaRecomendacion
	 * @param rutaRecomendacion ruta del archivo con ratings a analizar para 
	 * 		  generar la recomendaci�n
	 * @param n el n�mero de pel�culas recomendadas por cada una de las 
	 *        pel�culas presentes en el archivo rutaRecomendacion
	 * @return retorna un listado de n pel�culas recomendadas por cada una de 
	 * las pel�culas presentes en el archivo rutaRecomendaci�n. Para cada 
	 * pel�cula analizada se retorna una lista de n pel�culas que tengan dentro 
	 * de sus g�neros el g�nero principal de la pel�cula analizada y que la 
	 * diferencia entre su rating promedio y el rating asignado no sea mayor 
	 * a 0.5. El listado de pel�culas debe estar ordenado de menor a mayor 
	 * diferencia absoluta entre el rating asignado por el usuario y el rating promedio 
	 */
	public ILista<VOPeliculaPelicula> recomendarPeliculasSR(String rutaRecomendacion, int n)
	{
		
	}
	
	/**
	 * Retorna una lista con las calificaciones de una pel�cula determinada
	 * ordenadas por la fecha de creaci�n del rating (unix timestamp)
	 * @param idPelicula pel�cula para la cual se quieren obtener sus ratings
	 * @return lista con los ratings de la pel�cula idPelicula ordenada de 
	 *         m�s reciente a menos reciente
	 */
	public ILista<VORating> ratingsPeliculaSR (long idPelicula)
	{
		
	}
	
	/**
	 * Retorna una lista con los n usuarios m�s activos por cada uno de los g�neros
	 * presentes en las pel�culas
	 * @param n tama�o de la lista de usuarios
	 * @return una lista por cada g�nero con los n usuarios m�s activos
	 */
	public ILista<VOGeneroUsuario> usuariosActivosSR(int n)
	{
		
	}
	
	
	/**
	 * Retorna la lista total de usuarios presentes en el sistema ordenada por los siguientes criterios<br>
	 * <ol> 
	 * <li> Fecha de primer rating realizado
	 * <li> Total de ratings
	 * <li> Id de usuario
	 * </ol>
	 * @return Lista de usuarios ordenada por los criterios enunciados
	 */
	public ILista<VOUsuario> catalogoUsuariosOrdenadoSR()
	{
		ordenarUsuarios(0,usuarios.darNumeroElementos()-1);
		return usuarios;
	}
	
	/*private void ordenarUsuarios(int lo, int hi) {
		if(hi<=lo) return;	
		int rta=0;
		int i=lo;int j=hi+1; 
		VOUsuario v = usuarios.darElemento(lo);
		while(true)
		{
			while(usuarioMenor(usuarios.darElemento(i++),v)) if(i==hi) break;
			while(usuarioMenor(v,usuarios.darElemento(--j))) if(j==lo) break;
			if(i<=j) break;
			
		}
		ordenarUsuarios(lo,j-1);
		ordenarUsuarios(j+1,hi);
		intercambiar(usuarios,i,j);
	}*/


	/**
	 * Retorna la lista de pel�culas de tama�o n con m�s tags por cada uno de los g�neros presentes en el sistema
	 * @param n tama�o de las listas de cada g�nero
	 * @return  listado de pel�culas con m�s tags por cada g�nero
	 */
	public ILista<VOGeneroPelicula> recomendarTagsGeneroSR(int n)
	{
<<<<<<< HEAD
		ListaDobleEncadenada<VOGeneroPelicula> x = new ListaDobleEncadenada<VOGeneroPelicula>();
=======
		ListaDobleEncadenada<VOGeneroPelicula> x = new ListaDobleEncadenada<VOGeneroPelicula>();
>>>>>>> jfgc
		Iterator<VOGeneroPelicula> iter=generoPeliculas.iterator();
		while(iter.hasNext())
		{
			VOGeneroPelicula temp =iter.next();
			Iterator<VOPelicula> t =temp.getPeliculas().iterator();
			int i=0;
			while(t.hasNext() && i<n)
			{
				VOPelicula p =t.next();
			}
		}
		return generoPeliculas;
	}
	
	/**
	 * Retorna la lista de tags por cada usuario, por cada uno de los g�neros de pel�culas para las que ha realizado tags
	 * @return lista de tags de cada uno de los g�neros, por cada usuario 
	 */
	public ILista<VOUsuarioGenero> opinionTagsGeneroSR()
	{
		
	}
	
	/**
	 * Retorna una lista en donde en cada posici�n est� la pel�cula analizada y una lista de usuarios con opiniones similares a las registradas en el archivo rutaRecomendacion. 
	 * @param rutaRecomendacion ruta del archivo con ratings a analizar para 
	 * 		  generar la recomendaci�n
	 * @param n N�mero de usuarios recomendados por cada pel�cula
	 * @return Retorna una lista en donde en cada posici�n est� la pel�cula analizada y una lista de usuarios con opiniones similares, el listado de usuarios de cada pel�cula esta ordenado de menor a mayor diferencia.
	 */
	public ILista<VOPeliculaUsuario> recomendarUsuariosSR(String rutaRecomendacion, int n)
	{
		
	}
	
	/**
	 * Retorna una lista con todos los tags de una pel�cula determinada
	 * @param idPelicula pel�cula que tiene los tags a buscar
	 * @return tags de la pel�cula idPelicula, ordenados de m�s a menos reciente por su timestamp
	 */
	public ILista<VOTag> tagsPeliculaSR(int idPelicula)
	{
		Iterator<VOPelicula> iter = misPeliculas.iterator();
		VOPelicula pel=null;
		while(iter.hasNext())
		{
			VOPelicula temp =iter.next();
			if(temp.getMovieID()==idPelicula)
			{
				pel=temp;
				break;
			}
		}
		
<<<<<<< HEAD
		ILista<VOTag> asociados=new ListaDobleEncadenada<VOTag>();
=======
		ILista<VOTag> asociados=new ListaDobleEncadenada<VOTag>();
>>>>>>> jfgc
		ILista<String> str = pel.getTagsAsociados();
		Iterator<String> s=str.iterator();
		Iterator<VOTag> t= tags.iterator();
		while(s.hasNext())
		{
			String cmp=s.next();
			while(t.hasNext())
			{
				VOTag tag = t.next();
				if(tag.getTag().equals(cmp))
				{
					asociados.agregarElementoFinal(tag);
					break;
				}
			}
		}
		for(int i=1; i<asociados.darNumeroElementos();i++)
		{
			for(int j=i;j>=0;j--)
			{
				if(asociados.darElemento(j).getTimestamp()>asociados.darElemento(j-1).getTimestamp())
				{
					if(asociados.darElemento(j).equals(asociados.))
					intercambiar(asociados,j,j-1);
				}
			}
		}
		return asociados;
	}
	
	/**
	 * Agregar una nueva operacion en el sistema
	 * @param nomOperacion nombre de la operaci�n
	 * @param tinicio tiempo de inicio de la operacion
	 * @param tfin tiempo final de la operacion
	 */
	public void agregarOperacionSR(String nomOperacion, long tinicio, long tfin)
	{
		
	}
	
	/**
	 * Retorna una lista de operaciones realizadas sobre el sistema 
	 * @return lista de operaciones realizadas por el sistema
	 */
	public ILista<VOOperacion> darHistoralOperacionesSR()
	{
		
	}
	
	/**
	 * Limpia la lista de operaciones realizadas sobre el sistema 
	 * 
	 */
	public void limpiarHistorialOperacionesSR()
	{
		
	}
	
	/**
	 * Retorna una lista de las �ltimas n operaciones realizadas sobre el sistema
	 * @param n n�mero de operaciones
	 * @return lista de las �ltimas n operaciones realizadas en el orden inverso de ejecuci�n
	 */
	public ILista<VOOperacion> darUltimasOperaciones(int n)
	{
		
	}
	
	/**
	 * Borra las �ltimas n operaciones realizadas sobre el sistema
	 * @param n n�mero de operaciones
	 */
	public void borrarUltimasOperaciones(int n)
	{
		
	}
	
	/**
	 * Agrega una pel�cula al cat�logo de pel�culas
	 * @param titulo titulo de la pel�cula
	 * @param agno A�o de publicaci�n de la pel�cula
	 * @param generos G�neros que tiene la pel�cula
	 * @return Id de la pel�cula asignado por el sistema
	 */
	public long agregarPelicula(String titulo, int agno, String[] generos)
	{
		VOPelicula a= new VOPelicula();
		a.setTitulo(titulo);
		a.setAgnoPublicacion(agno);
		ListaDobleEncadenada<String> gen= new ListaDobleEncadenada<String>();
		for(int i=0; i<generos.length;i++)
		{
			gen.agregarElementoFinal(generos[i]);
		}
		a.setGenerosAsociados(gen);
		misPeliculas.agregarElementoFinal(a);
	}
	
	/**
	 * Agrega un nuevo rating al historial del sistema de recomendaci�n
	 * @param idUsuario usuario del rating
	 * @param idPelicula pel�cula a la cual se le asigna el rating
	 * @param rating valoraci�n de la pel�cula
	 */
	public void agregarRating(int idUsuario, int idPelicula, double rating)
	{
		VORating r= new VORating();
		r.setIdUsuario(idUsuario);
		r.setIdPelicula(idPelicula);
		r.setRating(rating);
		ratings.agregarElementoFinal(r);
	}
	//------------------
	//Completar método
	//-----------------
	private void intercambiar(ListaDobleEncadenada l, int i, int j)
	{
		
		
	}
	
	
	/*private boolean usuarioMenor(VOUsuario v, VOUsuario w)
	{
		if(v.getPrimerTimestamp()<w.getPrimerTimestamp())
		{
			return true;
		}
		else if(v.getPrimerTimestamp()==w.getPrimerTimestamp())
		{
			if(v.getNumRatings()<w.getNumRatings()) return true;
			else if (v.getNumRatings()==w.getNumRatings())
			{
				if(v.getIdUsuario()<w.getIdUsuario())
				{
					return true;
				}
			}
		}
		return false;
	}*/
	
	
	public static void main(String[] args) {
		SistemaRecomendacionPeliculas sr = new SistemaRecomendacionPeliculas();
		sr.crearNuevoSR();
		sr.cargarPeliculasSR("./data/movies.csv");
		sr.cargarRatingsSR("./data/ratings.csv");
		 sr.cargarTagsSR("./data/tags.csv");
		 Iterator<VOUsuarioGenero> i= sr.usuariosGenero.iterator();
		 while(i.hasNext())
		 {
			 VOUsuarioGenero g=i.next();
			 if(g.getIdUsuario()==15)
			 {
				 Iterator<VOGeneroTag> u=g.getListaGeneroTags().iterator();
				 while(u.hasNext())
				 {
					 VOGeneroTag x=u.next();
					 System.out.println(x.getGenero());
				 }
			 }
		 }
		 System.out.println(sr.sizeMoviesSR());
		 System.out.println(sr.sizeTagsSR());
		 System.out.println(sr.sizeUsersSR());
	}
	
}
