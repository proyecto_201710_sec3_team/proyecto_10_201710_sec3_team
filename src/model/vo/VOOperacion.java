package model.vo;

import model.data_structures.ILista;

public class VOOperacion {
	
	//--------------
	//Constantes
	//---------------
	
	public static final String CARGAR_PEL="cargarPeliculasSR";
	
	public static final String CARGAR_RAT="cargarRatingsSR";

	public static final String CARGAR_TAG="cargarTagsSR";
	
	public static final String SIZE_PEL="sizeMoviesSR";
	
	public static final String SIZE_USU="sizeUsersSR";
	
	public static final String SIZE_TAG= "sizeTagsSR";
	
	public static final String PEL_POPUL="peliculasPopularesSR ";
	
	public static final String CAT_PEL_ORDENADO="catalogoPeliculasOrdenadoSR";
	
	public static final String RECOMENDAR_GENERO="recomendarGeneroSR";
	
	public static final String OPINION_RAT_GEN="opinionRatingsGeneroSR";
	
	public static final String RECOMENDAR_PEL="recomendarPeliculasSR";
	
	public static final String RATINGS_PEL="ratingsPeliculaSR";
	
	public static final String USUARIOS_ACTIVOS="usuariosActivosSR";
	
	public static final String CAT_USUARIOS_ORDENADO="catalogoUsuariosOrdenadoSR";
	public static final String RECOMENDAR_TAG_GENERO="recomendarTagsGeneroSR";
	
	public static final String OPINION_TAG_GENERO="opinionTagsGeneroSR";
	
	public static final String RECOMENDAR_USUARIOS="recomendarUsuariosSR";
	
	public static final String TAGS_PEL="tagsPeliculaSR";
	
	public static final String AGREGAR_OPERACION="agregarOperacionSR";
	
	public static final String HISTORIAL="darHistoralOperacionesSR";
	
	public static final String LIMPIAR_HISTORIAL="limpiarHistorialOperacionesSR";
	
	public static final String N_OPERACIONES="darUltimasOperaciones";
	
	public static final String BORRAR_N_OPERACIONES="borrarUltimasOperaciones";
	
	public static final String AGREGAR_PEL="agregarPelicula";
	
	public static final String AGREGAR_RATING="agregarRating";
	
	
	private String operacion;
	private long timestampInicio;
	private long timestampFin;
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public long getTimestampInicio() {
		return timestampInicio;
	}
	public void setTimestampInicio(long timestampInicio) {
		this.timestampInicio = timestampInicio;
	}
	public long getTimestampFin() {
		return timestampFin;
	}
	public void setTimestampFin(long timestampFin) {
		this.timestampFin = timestampFin;
	}

}
