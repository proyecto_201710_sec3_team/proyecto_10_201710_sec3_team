import junit.framework.TestCase;
import model.logic.SistemaRecomendacionPeliculas;

public class pruebasCargaSR extends TestCase {

	private SistemaRecomendacionPeliculas sr;
	
	private final static String PRUEBA="./data/PruebasCarga/";
	
	private static final String RATING_B=PRUEBA+"RatingCorrecto.csv";
	private static final String RATING_M=PRUEBA+"RatingIncorrecto.csv";
	private final static String TAG_B=PRUEBA+"TagCorrecto.csv";
	private final static String TAG_M=PRUEBA+"TagIncorrecto.csv";
	private final static String MOVIE_B=PRUEBA+"MovieCorrecto.csv";
	private final static String MOVIE_M=PRUEBA+"MovieIncorrecto.csv";
	
	public void setupEscenario()
	{
		sr=new SistemaRecomendacionPeliculas();
		sr.crearNuevoSR();
	}
	
	public void testCargaErronea()
	{
		try
		{
			setupEscenario();
			assertFalse("No debería cargar el archivo",sr.cargarPeliculasSR(MOVIE_M));
			assertFalse("No debería cargar el archivo",sr.cargarRatingsSR(RATING_M));
			assertFalse("No debería cargar el archivo",sr.cargarTagsSR(TAG_M));
			assertFalse("No debería cargar el archivo",sr.cargarRatingsSR(TAG_B));
			assertFalse("No debería cargar el archivo",sr.cargarTagsSR(MOVIE_B));
			assertFalse("No debería cargar el archivo",sr.cargarPeliculasSR(RATING_B));
			
			assertEquals("El tamaño no es igual",0,sr.sizeMoviesSR());
			assertEquals("El tamaño no es igual",0,sr.sizeTagsSR());
			assertEquals("El tamaño no es igual",0,sr.sizeUsersSR());
		}
		catch (Exception e)
		{
			//
		}
		
	}
	
	public void testCargaCorrecta()
	{
		setupEscenario();
		assertTrue("Debería cargar el archivo",sr.cargarPeliculasSR(MOVIE_B));
		assertTrue("Debería cargar el archivo",sr.cargarRatingsSR(RATING_B));
		assertTrue("Debería cargar el archivo",sr.cargarTagsSR(TAG_B));
		
		assertEquals("El tamaño no es igual",342,sr.sizeMoviesSR());
		assertEquals("El tamaño no es igual",152,sr.sizeTagsSR());
		assertEquals("El tamaño no es igual",6,sr.sizeUsersSR());

	}
}
